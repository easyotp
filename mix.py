#!/bin/env python
# Created:20080610
# By Jeff Connelly
#
# Mix random data, compressing low-entropy into high-entropy

import hashlib
import sys
import math

hash = hashlib.sha512

# in / out
ratio = 2

if len(sys.argv) > 1:
    ratio = float(sys.argv[1])

hash_size = len(hash("").digest())
bytes_to_read = int(math.floor(hash_size * ratio))

while True:
    input = sys.stdin.read(bytes_to_read)
    if len(input) == 0:
        break
    output = hash(input).digest()
    sys.stdout.write(output)
    if len(input) < bytes_to_read:
        break

