#!/bin/env python
# Created:20080603
# By Jeff Connelly
#
# CLI for EasyOTP

import SecureMail
import cotp
import getpass
import sys
import os
import time

ms = None

def help():
    print """commands:
h   Help
l   List messages
q   Quit
r#  Read message number #
w   Write a new message
x#  Replace what an old message decrypts to
"""

def read_message():
    subject = raw_input("Subject: ")
    body = multiline_input("Enter body, ending with '.' on a line by itself.")

    return subject, body

def multiline_input(prompt=None):
    """Read possibly multiple lines of input, terminated by '.'."""
    if prompt is not None:
        print prompt
    inp = ""
    while True:
        line = raw_input()
        if line == ".":
            break
        inp += line + "\r\n"
    return inp

def main():
    global ms
    ms = SecureMail.SecureMail()
    msgs = []

    while True:
        try:
            line = raw_input(">> ")
        except EOFError:
            break
        if len(line.strip()) == 0:
            continue

        if line[0] == "h":
            help()
        elif line[0] == "l":
            print "Fetching messages..."
            i = 0
            for m in ms:
                print i, m["sender"], m["subject"]
                i += 1
        elif line[0] == "q":
            raise SystemExit
        elif line[0] == "w":
            to = raw_input("To: ")

            subject, body = read_message()
            print ms.send(to, subject, body)
        elif line[0] == "r":
            try:
                num = int(line[1:])
            except:
                print "Usage: r#"
                continue
            try:
                msg = ms[num]
            except:
                print "Bad message number: %s" % (num,)
                print "Are you sure it is listed with 'l'?"
                print "Message count: %s" % (len(msgs),)
                continue

            print "From: %s" % (msg["sender"],)
            print "Subject: %s" % (msg["subject"],)
            print msg["body"]
        elif line[0] == "x":
            try:
                num = int(line[1:])
            except:
                print "Usage: x#"
                continue
            print "After-The-Fact Message Replacement"
            subject, body = read_message()
            print ms.replace(num, subject, body)
        else:
            print "Unknown command, type h for help"
        
if __name__ == "__main__":
    main()

