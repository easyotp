#!/bin/env python
# Created:20080603
# By Jeff Connelly
#
# GUI for EasyOTP TODO

from Tkinter import *
import cotp
import MailServers

class App(object):
    def __init__(self, master):
        frame = Frame(master)
        frame.pack(expand=True, fill=BOTH)

        self.read = Button(frame, text="Read", command=self.read)
        self.read.pack(side=LEFT)

        self.write = Button(frame, text="Write", command=self.write)
        self.write.pack(side=LEFT)

        self.quit = Button(frame, text="Quit", command=frame.quit)
        self.quit.pack(side=LEFT)

    def read(self):
        print "read"
    
    def write(self):
        print "write"

root = Tk()
app = App(root)
root.title("EasyOTP")
root.mainloop()

