/** Practical One-time Pad Library
 *
 * Created:20080514
 * By Jeff Connelly
 */

#define MARKER_TO               "to:"
#define MARKER_BEGIN            "--EMOTP_BEGIN--"
#define MARKER_END              "--EMOTP_END--"

/** Pad messages to a multiple of this many bytes, to hide the true
 * message length. Higher obscures more information but burns more pad.
 * For the best security, this should be the length of the longest message
 * you'll ever send, then all messages will be the same length and an attacker
 * can tell nothing by the length of the message!.
 */
#define PADDING_MULTIPLE        1024

#define OFFSET_FILE_EXTENSION   ".off"
#define OFFSET_SIZE             11      /* strlen("4294967296") + 1 */
#define PAD_NAME_LENGTH         4
#define MAX_CONFIG_LINE         1024    /* should be enough, >MAX_PATH */

/* Define to get warnings if a message is replayed. */
/*#define WARN_REPLAY*/

/* One-time pad. */
typedef struct _PAD {
    char *local_filename;
    char *name;
    FILE *fp;
    struct _PAD *next;
    /* Use read_offset() and write_offset() to access offset. */
} PAD;


/** Packaged up encrypted message, ready for transport. */
typedef struct _MESSAGE {
    unsigned long offset;
    PAD *pad;
    unsigned long length;
    char *cipher_text;
} MESSAGE;

void load_config(char *config_filename);
void show_pads();
FILE *open_offset_file(PAD *p, char *mode);
unsigned long read_offset(PAD *p);
void write_offset(PAD *p, unsigned long offset);
void load_pad(char *local_filename, char *pad_name);
void free_pads();
MESSAGE *unpackage(char *input);
void free_message(MESSAGE *);
char *otp_encrypt(char *input, unsigned int length, char *to, unsigned int *out_length);
unsigned int otp_decrypt(char *input, char **out);
unsigned int otp_replace(char *input, char *with);

