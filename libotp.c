/** Practical One-time Pad Library
 *
 * Created:20080514
 * By Jeff Connelly
 */

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <string.h>
#include <ctype.h>

#include "libotp.h"
#include "base64.h"

PAD *pads = NULL;

/** Show a list of all loaded pads. */
void show_pads()
{
    PAD *p;

    for (p = pads; p; p = p->next) {
        printf("Pad: %s: %s\n", p->name, p->local_filename);
    }
}

/** Open a companion offset file for given pad. Caller must close. 
 *
 * @param mode Either "rt" or "wt", for reading or writing. 
 */
FILE *open_offset_file(PAD *p, char *mode)
{
    FILE *ofp;
    char *offset_filename;
    size_t filename_length;

    /* Offset is stored in a separate file; pad plus OFFSET_FILE_NAME_EXTENSION. */
    filename_length = strlen(p->local_filename) + strlen(OFFSET_FILE_EXTENSION) + 1;
    offset_filename = malloc(filename_length);
    if (!offset_filename) {
        perror("malloc");
        exit(EX_UNAVAILABLE);
    }
    snprintf(offset_filename, filename_length, "%s" OFFSET_FILE_EXTENSION, p->local_filename);

    /* Read offset from file. */
    ofp = fopen(offset_filename, mode);
    if (!ofp) {
        fprintf(stderr, "opening offset file %s failed\n", offset_filename);
        perror("fopen");
        free(offset_filename);
        exit(EX_IOERR);
    }
    free(offset_filename);

    return ofp;
}

/** Read the pad offset of a given pad. */
unsigned long read_offset(PAD *p)
{
    FILE *ofp;
    char buffer[OFFSET_SIZE];
    unsigned long offset;

    ofp = open_offset_file(p, "rt");

    memset(buffer, 0, OFFSET_SIZE);
    if (fread(buffer, 1, OFFSET_SIZE - 1, ofp) < 1) {
        fprintf(stderr, "could not read offset file for %s\n", p->local_filename);
        exit(EX_IOERR);
    }

    if (fclose(ofp) != 0) {
        fprintf(stderr, "error closing offset file for reading for %s\n", p->local_filename);
        perror("fclose");
        exit(EX_IOERR);
    }

    /* We finally got it! */
    offset = strtoul(buffer, NULL, 10);

    return offset;
}

/** Write the pad offset to the given pad. */
void write_offset(PAD *p, unsigned long offset)
{
    FILE *ofp;
    char buffer[OFFSET_SIZE];

    ofp = open_offset_file(p, "wt");

    memset(buffer, 0, OFFSET_SIZE);
    snprintf(buffer, OFFSET_SIZE - 1, "%ld", offset);
    
    if (fwrite(buffer, strlen(buffer), 1, ofp) != 1) {
        fprintf(stderr, "write error saving offset %ld for %s\n", offset, p->local_filename);
        exit(EX_IOERR);
    }


    if (fclose(ofp) != 0) {
        fprintf(stderr, "error closing offset file for writing for %s\n", p->local_filename);
        perror("fclose");
        exit(EX_IOERR);
    }
}

/** Load a pad file from disk, adding to 'pads' global. */
void load_pad(char *local_filename, char *pad_name)
{
    FILE *fp;
    PAD *new_pad;

    fp = fopen(local_filename, "rb+");
    if (!fp) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    new_pad = malloc(sizeof(PAD));
    if (!new_pad) {
        perror("malloc");
        exit(EX_UNAVAILABLE);
    }

    new_pad->local_filename = strdup(local_filename);
    new_pad->name = strdup(pad_name);
    new_pad->fp = fp;
    new_pad->next = NULL;

    /* Add to linked list. */
    if (!pads) {
        pads = new_pad;
    } else {
        PAD *p, *tail;

        /* Find tail */
        for (p = pads; p; p = p->next)
            tail = p;
        tail->next = new_pad;
    }
}

/** Read a line from a file, up to max characters.
 * Does not place \n in line string.
 *
 * Based on http://www.eskimo.com/~scs/cclass/notes/sx6c.html 
 *
 * @return Line length (0 for empty), or EOF for end-of-file.
 */
static int getline(FILE *fp, char *line, unsigned int max)
{
    int i, c;

    i = 0;
    max -= 1;

    while ((c = fgetc(fp)) != EOF) {
        if (c == '\n')
            break;
        if (i < max)
            line[i++] = c;

    }

    if (c == EOF && i == 0)
        return EOF;
 
    line[i] = '\0';
    return i;
}

void load_config(char *config_filename)
{
    FILE *cfg;
    char line[MAX_CONFIG_LINE];

    cfg = fopen(config_filename, "rt");
    if (!cfg) {
        fprintf(stderr, "failed to open configuration file %s\n", config_filename);
        perror("fopen config file");
        exit(EX_IOERR);
    }

    while(getline(cfg, line, MAX_CONFIG_LINE) != EOF) {
        char *pad_name, *pad_filename;

        /* Line is: (pad name)=(local filename) */
        pad_name = strtok(line, "=");
        if (!pad_name)
            continue;
        pad_filename = strtok(NULL, "=");

        /* TODO: separate send and recv filename, separated with : */

        load_pad(pad_filename, pad_name);
    }
}

/** Find pad with given name. */
PAD *find_pad(char *pad_name)
{
    PAD *p;

    /* Null or blank pad = default (first) pad. */
    if (!pad_name || !strcmp(pad_name, ""))
        return pads;

    for (p = pads; p; p = p->next) {
        if (!strcmp(p->name, pad_name))
            return p;
    }

    return NULL;
}

/** Close all pads and free allocated memory. */
void free_pads()
{
    PAD *p, *next;

    for (p = pads; p; p = next) {
        free(p->name);
        free(p->local_filename);
        fclose(p->fp);
        next = p->next;
        free(p);
    }
}

void free_message(MESSAGE *msg)
{
    free(msg->cipher_text);
    free(msg);
}

/** Unpackage a message packaged for transport. 
 *
 * Caller must free_message(). */
MESSAGE *unpackage(char *input)
{
    MESSAGE *msg;
    unsigned int at, i, j;
    int b64_ret;
    char *s, *end, *b64_ct;
    char pad_name[PAD_NAME_LENGTH];

    msg = malloc(sizeof(MESSAGE));
    if (!msg) {
        perror("malloc");
        exit(EX_UNAVAILABLE);
    }

    /** Format <v0.7 (pad name is unspecified; use default):
     * MARKER_BEGIN + offset + comma
     * base64'd data
     * MARKER_END
     *
     * Format >=0.7:
     * MARKER_BEGIN + offset + comma + pad_name + comma
     * base64'd data
     * MARKER_END
     */

    at = 0;
    
    /* Locate where the message begins. */
    s = strstr(input, MARKER_BEGIN);
    if (!s) {
        fprintf(stderr, "unpackage: input |%s| lacks beginning marker %s\n",
                input, MARKER_BEGIN);
        exit(EX_DATAERR);
    }

    /* ...and ends. */
    end = strstr(input, MARKER_END);
    if (!end) {
        fprintf(stderr, "unpackage: input |%s| lacks ending marker %s\n",
                input, MARKER_END);
        exit(EX_DATAERR);
    }

    s += strlen(MARKER_BEGIN);
    msg->offset = strtoul(s, &s, 10);

    /* Move after mandatory comma. */
    if (s[0] != ',') {
        fprintf(stderr, "unpackage: missing comma after offset, at |%s|, input |%s|\n",
                s, input);
        exit(EX_DATAERR);
    }
    ++s;
    
    memset(pad_name, 0, PAD_NAME_LENGTH);
    
    /* Includes pad name? */
    if (s[0] != '\n' && s[0] != '\r') {
        unsigned int i;

        i = 0;
        /* v0.7+ message, includes pad name */
        while(s[0] != '\n' && s[0] != '\r' && s[0] != ',' && s < end) {
            pad_name[i] = s[0];
            ++s;
            ++i;
            if (i > PAD_NAME_LENGTH) {
                fprintf(stderr, "unpackage: pad name length > maximum %d, in input |%s|\n",
                        PAD_NAME_LENGTH, input);
                exit(EX_DATAERR);
            }
        }
    }

    msg->pad = find_pad(pad_name);
    if (!msg->pad) {
        fprintf(stderr, "No such pad by name '%s'\n", pad_name);
        exit(EX_DATAERR);
    }

    /* Go to next line */
    while((s[0] == '\n' || s[0] == '\r' || s[0] == ',') && (s < end))
        ++s;

    /* Extract base64 data from end of message, removing whitespace. */
    b64_ct = malloc(end - s + 4);
    if (!b64_ct) {
        perror("malloc b64_ct");
        exit(EXIT_FAILURE);
    }

    i = j = 0;
    do
    {
        if (!isspace(s[i]))
            b64_ct[j++] = s[i];
    } while(i++ < end - s);
    b64_ct[i - 1] = '\0';
    b64_ct[i    ] = '\0';
    b64_ct[i + 1] = '\0';
    b64_ct[i + 2] = '\0';


    /* Add base64 padding, if needed */
    i = strlen(b64_ct) % 4;
    while(i--) 
        b64_ct[strlen(b64_ct)] = '=';

    b64_ct[strlen(b64_ct) + 1] = '\0';

    /* Decode base64 */
    msg->cipher_text = malloc(strlen(b64_ct));

    /* Decode base64. This may return -1 if it fails, so assign to a 
     * signed variable. */
    b64_ret = base64_decode(b64_ct, msg->cipher_text);

    if (b64_ret < 0) {
        fprintf(stderr, "unpackage: failed to base64 decode |%s|\n", b64_ct);
        exit(EX_DATAERR);
    }

    msg->length = b64_ret;

    free(b64_ct);

    return msg;
}

/** Apply Vernam cipher (XOR) with given input and text.
 *
 * @param input What to XOR with the pad, either ciphertext to decrypt or plaintext to encrypt.
 *
 * @param length Length of input, in bytes.
 *
 * @param pad Pad to XOR 'input' with offset.
 *
 * @param offset Offset within pad to start XOR at.
 *
 * @return Pad XOR'd with input. Caller must free.
 *  The return value is the same length as 'length'. However, it is null-terminated
 *  in an extra byte for convenience. There may be embedded nulls.
 */
static char *xor_with_pad(char *input, unsigned long length, PAD *pad, unsigned long offset)
{
    char *pad_data, *out;
    unsigned int i;

    /* Seek to area of pad we're using. */
    if (fseek(pad->fp, offset, SEEK_SET) < 0) {
        fprintf(stderr, "failed to seek in pad %s to %ld\n",
                pad->name, offset);
        perror("fseek");
        exit(EXIT_FAILURE);
    }

    /* Read pad. */
    pad_data = malloc(length);
    if (!pad_data) {
        perror("malloc pad data");
        exit(EX_UNAVAILABLE);
    }
    if (fread(pad_data, length, 1, pad->fp) < 1) {
        fprintf(stderr, "read pad %s, offset %ld, length %ld failed",
                pad->name, offset, length);
        exit(EXIT_FAILURE);
    }

    /* Apply XOR to give output.  */
    out = malloc(length + 1);
    if (!out) {
        perror("malloc output buffer");
        exit(EX_UNAVAILABLE);
    }

    for (i = 0; i < length; ++i) {
        out[i] = pad_data[i] ^ input[i];
    }
    free(pad_data);

    /* Null-terminate output for convenience when using text-only messages.
     * The output is still of length msg->length; use msg->length when decrypting
     * binary messages. */
    out[length] = 0;

    return out;
}


/** Decrypt a packaged message and return plaintext. 
 *
 * @param out Pointer to pointer which is set to the output. Caller must free.
 *  There is an extra null at the end of this buffer, not part of the output,
 *  but there may be embedded nulls if binary data is encoded. If this is expected,
 *  use the return value:
 *
 * @return Length of message, in bytes.
 */
unsigned int otp_decrypt(char *input, char **out)
{
    MESSAGE *msg;
    unsigned int length;

    msg = unpackage(input);

    if (msg->offset < read_offset(msg->pad)) {
#ifdef WARN_REPLAY
        fprintf(stderr, "** warning: this is an old message! possible replay attack: %ld < %ld\n",
                msg->offset, read_offset(msg->pad));
#endif
    }

    length = msg->length;
    *out = xor_with_pad(msg->cipher_text, msg->length, msg->pad, msg->offset);
    free_message(msg);
    
    return length;
}

/** Replace part of the pad corresponding to an encrypted message 
 * so that it encrypts to something else.
 *
 * @param input A packaged, encrypted message.
 * @param with What to make 'input' decrypt to by changing the pad.
 */
unsigned int otp_replace(char *input, char *with)
{
    unsigned int length;
    int i;
    char c;

    MESSAGE *msg;
    msg = unpackage(input);

    if (fseek(msg->pad->fp, msg->offset, SEEK_SET) < 0) {
        perror("fseek");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < msg->length; ++i) {
        char with_c;

        /* What to make the message decrypt to. */
        if (i < strlen(with))
            with_c = with[i];
        else
            with_c = '\0';

        c = msg->cipher_text[i] ^ with_c;

        fputc(c, msg->pad->fp);
    }

    return length;
}

/** Package up a message for transport.
 *
 * @return Packaged message. Caller frees.
 */
char *package(MESSAGE *msg, unsigned int *out_length)
{
    char *out;
    char *b64_data;

    /* Space for markers, and 4/3 of plaintext since base64'd. */
    *out_length = strlen(MARKER_BEGIN) + 1 + OFFSET_SIZE + 1 + 
        1 + msg->length * 4 / 3 + 1 + strlen(MARKER_END) + 1 + 1 + 1;
    out = malloc(*out_length);
    if (!out) {
        perror("malloc otp_decrypt output");
        exit(EX_DATAERR);
    }

    base64_encode(msg->cipher_text, msg->length, &b64_data);

    snprintf(out, *out_length, "%s%ld,%s,\n"
            "%s\n"
            "%s\n",
            MARKER_BEGIN,
            msg->offset,
            msg->pad->name,
            b64_data,
            MARKER_END);

    return out;
}

/** Encrypt and package a message. 
 *
 * @return Encrypted, packaged message. Caller must free.
 */
char *otp_encrypt(char *input, unsigned int length, char *to, unsigned int *out_length)
{
    MESSAGE msg;
    char *out;
    char *input_padded;
    unsigned int length_padded;

    msg.pad = find_pad(to);
    if (!msg.pad) {
        fprintf(stderr, "otp_encrypt: no such pad %s\n", to);
        exit(EX_DATAERR);
    }

    /* Round up to nearest multiple of PADDING_MULTIPLE. Mitigates analysis
     * based on message length. */
    length_padded = length + (PADDING_MULTIPLE - length % PADDING_MULTIPLE);
    input_padded = malloc(length_padded);
    if (!input_padded) {
        perror("malloc input padded");
        exit(EXIT_FAILURE);
    }
    
    /* input_padded is input, padded with 0s. */
    memset(input_padded, 0, length_padded);
    strncpy(input_padded, input, length);

    /* Use current offset. */
    msg.offset = read_offset(msg.pad);
    msg.length = length_padded;
    msg.cipher_text = xor_with_pad(input_padded, length_padded, msg.pad, msg.offset);

    /* Advance pad. */
    write_offset(msg.pad, msg.offset + msg.length);

    out = package(&msg, out_length);

    return out;
}
