#!/bin/env python
import popen2

def decode(ct):
    o, i, e = popen2.popen3("./cotp -d")
    i.write(ct)
    i.close()
    pt = o.read()
    errors = e.read()
    if errors:
        raise errors
    return pt

def replace(ct_plus_new):
    o, i, e = popen2.popen3("./cotp -r")
    i.write(ct_plus_new)
    i.close()
    pt = o.read()
    errors = e.read()
    if errors:
        raise errors
    return True     # success

# TODO: support padname
def encode(pt):
    o, i, e = popen2.popen3("./cotp -e")
    i.write(pt)
    i.close()
    ct = o.read()
    errors = e.read()
    if errors:
        raise errors
    return ct

if __name__ == "__main__":
    ct = encode("hello world")
    print ct
    pt = decode(ct)
    print pt
    assert pt == "hello world", "decode failed to match"

