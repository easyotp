/** Practical One-time Pad Library
 *
 * Created:20080514
 * By Jeff Connelly
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sysexits.h>

#include "libotp.h"

extern PAD *pads;

extern char *optarg;
extern int optind;

void test()
{
    char *o;
    unsigned int i;

    load_config("otp.conf");
    show_pads();

    /*
    printf("offset=%ld\n", read_offset(pads));
    write_offset(pads, 1213475);
    printf("offset=%ld\n", read_offset(pads));
    */

    otp_decrypt("--EMOTP_BEGIN--1213434,\n"
            "gK1O22FPbxLmxrROfFHDCsM1LTsOAjjbRlHVM1p+WG+s6yslYVfzvtc=\n"
            "--EMOTP_END--\n", &o);
    puts(o);
    free(o);

    otp_decrypt("--EMOTP_BEGIN--978,dc,\n"
            "hUZm1q0gX7pa6Alzbo9OZiT8wA==\n"
            "--EMOTP_END--\n", &o);
    puts(o);
    free(o);

    o = otp_encrypt("hello world", strlen("hello world"), "dc", &i);
    printf("encrypt = %s\n", o);
    free(o);

    otp_decrypt("junkasdsjdfldjf jdsfjunk--EMOTP_BEGIN--1213475,dc,\n"
            "v7jlXbCCvPv4S0k=\n"
            "--EMOTP_END--\n-tasjdktrailingjunkksjdjf", &o);
    puts(o);
    free(o);

    free_pads();
}

/* Operation mode */
#define ENCRYPT -1
#define AUTO     0
#define DECRYPT 1
#define REPLACE 2

void usage()
{
    fprintf(stderr, "usage: otp [-e | -d | ] [-t <pad_name>]\n"
            "\n"
            "Mode selection:\n"
            "-e     Encrypt\n"
            "-d     Decrypt\n"
            "-r     Replace what an existing encrypted message decrypts to\n"
            "Default is automatic.\n"
            "\n"
            "-t     Specify pad name to encrypt with. Default: first pad.\n");
    exit(EX_USAGE);
}

/* Read input data. 
 *
 * @param size Set to number of bytes read.
 *
 * @return Input, caller frees.
 */
char *read_input(unsigned int *size)
{
    unsigned int i, buffer_size;
    char *input;

    i = 0;
    buffer_size = 1024;
    input = malloc(buffer_size);
    if (!input) {
        perror("malloc input buffer");
        exit(EX_UNAVAILABLE);
    }

    while(!feof(stdin)) {
        input[i++] = fgetc(stdin);
        if (input[i - 1] == EOF)
            break;
        if (i >= buffer_size) {
            buffer_size += 1024;
            input = realloc(input, buffer_size);
            if (!input) {
                perror("realloc input buffer");
                exit(EX_UNAVAILABLE);
            }
        }
    }
    input[i - 1] = 0;
    *size = i - 1;

    return input;
}

int main(int argc, char **argv)
{
    int ch, mode, i;
    unsigned int length, output_length;
    char *to, *input, *output;

    mode = AUTO;
    to = NULL;

    while((ch = getopt(argc, argv, "erdt:")) != -1) {
        switch(ch)
        {
        case 'e': 
            mode = ENCRYPT;
            break;
        case 'd': 
            mode = DECRYPT;
            break;
        case 't':
            to = optarg;
            break;
        case 'r':
            mode = REPLACE;
            break;
        case '?':
        default:
            usage();
        }
    }
    argc -= optind;
    argv += optind;

    /* Automatic mode - guess based on magic markers.
     * Explicit modes still allowed so can encrypt data with
     * magic markers in it (if you ever want to). */
    if (mode == AUTO) {
        if (strstr(input, MARKER_BEGIN))
            mode = DECRYPT;
        else
            mode = ENCRYPT;
    }

    load_config("otp.conf");

    input = read_input(&length);

    if (mode == ENCRYPT) {
        output = otp_encrypt(input, length, to, &output_length);
    } else if (mode == DECRYPT) {
        output_length = otp_decrypt(input, &output);
    } else if (mode == REPLACE) {
        if (!strstr(input, MARKER_END)) {
            fprintf(stderr, "need encrypted pad, trailed by new message\n");
            exit(EXIT_FAILURE);
        }

        otp_replace(input, strstr(input, MARKER_END) + strlen(MARKER_END) + 1);
        output_length = 0;
    }

    for (i = 0; i < output_length; ++i)
        putchar(output[i]); 

    free(output);
    free_pads();

    return 0;
}

