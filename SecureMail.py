#!/bin/env python
# Created:20080603
# By Jeff Connelly
#
# Communicate with mail servers
#
import imaplib
import smtplib
import getpass
import email
import cotp
import threading
import Queue
import time

# Every X seconds, send a message, and queue messages to be sent at in this
# interval. 
#FILL_INTERVAL = 10
FILL_INTERVAL = None

# Real subject used for all encrypted messages
# Note: make a Gmail filter that filters (Secure Message) into the Secure tag
FAKE_SUBJECT = "(Secure Message)"

# Dummy message used in channel filling if there is nothing to send.
DUMMY_SUBJECT = "Filler Message"
# TODO: should be the person you're always sending to
FILLER_TO = "shellreef@gmail.com"

class SecureMail(threading.Thread):
    def __init__(self, username=None, password=None):
        """Prompt for login and password, or using stored values if possible, then login."""

        if username is None:
            try:
                username = file(".otplogin").read().strip()
            except:
                username = raw_input("Username: ")

        if password is None:
            try:
                password = file(".otppass").read().strip()
            except:
                password = getpass.getpass()

        threading.Thread.__init__(self)
        return self.login(username, password)


    def login(self, username, password):
        """Login to a Gmail account, IMAP and SMTP server, with login and password."""

        # IMAP SSL for incoming messages
        self.mail_in = imaplib.IMAP4_SSL("imap.gmail.com", 993)

        self.username = username

        try:
            typ, data = self.mail_in.login(username, password)
        except imaplib:
            raise "Login failure: %s" % (sys.exc_info(),)
        else:
            assert typ == "OK", "imap login returned: %s %s" % (status, message)
            print "Logged in:", typ, data

        # Always have "Secure" mailbox selected
        typ, num_msgs = self.mail_in.select(mailbox="Secure")
        if typ != "OK":
            raise ("imap select failure: %s %s" % (typ, num_msgs) +
                "The 'Secure' tag doesn't exist. Tag some of your EMOTP messages" +
                "using a new label, named 'Secure'")

        # Outgoing mail, SMTP server
        self.mail_out = smtplib.SMTP("smtp.gmail.com", 25)
        ret = self.mail_out.ehlo()
        assert ret[0] == 250, "SMTP server EHLO failed: %s" % (ret,)

        ret = self.mail_out.starttls()
        assert ret[0] == 220, "SMTP server STARTTLS failed: %s" % (ret,)

        ret = self.mail_out.ehlo()
        assert ret[0] == 250, "SMTP server EHLO over TLS failed: %s" % (ret,)

        ret = self.mail_out.noop()
        assert ret[0] == 250, "SMTP server NOOP failed: %s" % (ret,)

        ret = self.mail_out.login(username, password)
        assert ret[0] == 235, "SMTP server login failed: %s" % (ret,)
        print "Logged in to SMTP server: %s" % (ret,)

        # Start channel filler
        self.sendq = Queue.Queue()
        self.start()

    # Channel filling thread
    # Problem: computer needs to be online to fill channel!
    def run(self):
        if FILL_INTERVAL is None:
            print "Channel filling disabled - messages will send immediately"
            return

        while True:
            try:
                item = self.sendq.get_nowait()
            except Queue.Empty:
                print "Sending filler message"
                body = ""       # empty, they should have padding!
                to = FILLER_TO  # todo: find out other person's email
                subject = DUMMY_SUBJECT

                # Note: subject is encrypted and sent in body, so it is secure
                self.send_now(to, subject, body)
            else:
                print "Sending queued message now"
                to, subject, body = item
                self.send_now(to, subject, body)

            time.sleep(FILL_INTERVAL)




    def get_messages(self):
        msgs = []

        try:
            typ, all_msgs_string = self.mail_in.search(None, 'ALL')
        except imaplib.error, e:
            raise "imap search failed: %s" % (e,)

        all_msgs = all_msgs_string[0].split()
        for num in all_msgs:
            typ, body = self.mail_in.fetch(num, "(BODY[])")
            msg = email.message_from_string(body[0][1])

            enc_body = str(msg.get_payload())
            fake_subject = msg.get("Subject")   # not encrypted
            sender = msg.get("From")
            id = msg.get("Message-ID")

           #print 'Message %s\n%s\n' % (num, data[0][1])
            if "--EMOTP_BEGIN--" not in enc_body:
                continue

            subject_plus_body = cotp.decode(enc_body).strip()

            if "<body>" in subject_plus_body:
                # encrypted subject
                subject, body = subject_plus_body.split("<body>")
            else:
                subject, body = fake_subject, subject_plus_body

            if subject == DUMMY_SUBJECT:
                # ignore filler
                continue

            msgs.append({"body": body,
                "body-enc": enc_body,
                "fake-subject": fake_subject,
                "subject": subject,
                "sender": sender,
                "id": id,
                "num": num})

        return msgs

    def __iter__(self):
        """Fetch messages from server."""
        self.msgs = self.get_messages()
        return iter(self.msgs)

    def __getitem__(self, k):
        """Lookup a message of a given number. Messages 
        must have been previous fetched from server up by __iter__."""

        return self.msgs[k]

    def replace(self, k, subject, body):
        """Replace the message that 'k' decrypts to with 'new', by
        rewriting the pad. The same ciphertext now decrypts to 'new' instead
        of what it used to."""
        new = subject + "<body>" + body
        ret = cotp.replace(self.msgs[k]["body-enc"] + "\n" + new)

        # Re-decrypt on our side for convenience
        subject_plus_body = cotp.decode(self.msgs[k]["body-enc"]).strip()

        if "<body>" in subject_plus_body:
            # encrypted subject
            subject, body = subject_plus_body.split("<body>")
        else:
            subject, body = self.msgs[k]["fake-subject"], subject_plus_body

        self.msgs[k]["subject"] = subject
        self.msgs[k]["body"] = body

        return ret

    def send(self, to, subject, body):
        """Send, in a timeslot if channel filling is enabled, or immediately
        if channel filling is disabled."""
        if FILL_INTERVAL is None:
            print "Sending %s bytes now" % (len(body,))
            return self.send_now(to, subject, body)
        else:
            self.sendq.put((to, subject, body))
            print "Enqueued to send at next interval, pending: %s" % (self.sendq.qsize(),)
            return None

    def send_now(self, to, subject, body):
        """Send an encrypted message immediately."""
        from_address = "%s@gmail.com" % (self.username,)
        # TODO: encode with different pads based on 'to' email
        enc_body = cotp.encode(subject + "<body>" + body)
        return self.mail_out.sendmail(from_address,
            [to], 
            "\r\n".join([
            "From: %s" % (from_address,),
            "To: %s" % (to,),
            "Subject: %s" % (FAKE_SUBJECT,),
            "",
            enc_body]))

    def __del__(self):
        self.mail_in.close()
        self.mail_in.logout()
        self.mail_out.quit()

def main():
    ms = SecureMail("shellreef", getpass.getpass())

    for m in ms.get_messages():
        print m["sender"], m["subject"]

if __name__ == "__main__":
    main()

