CC=gcc
CFLAGS=-g -Wall -pedantic
LD=$(CC)
LDFLAGS=
RM=rm

EXE=cotp
SRCS=libotp.c cotp.c base64.c
OBJS=${SRCS:.c=.o}
HEADERS=libotp.h base64.h

.c.o:
	$(CC) $(CFLAGS) -c $<

all : $(EXE)

$(EXE) : $(OBJS)
	$(LD) -o $@ $(OBJS)

$(OBJS) : $(HEADERS)

clean:
	-$(RM) -f $(EXE) $(OBJS)
